package dungeon

import (
	"image"
	"math"
	"math/rand"
)

type BSPTreeMapGenerator struct {
	width           int
	height          int
	maximumLeafSize int
	minimumLeafSize int
	maximumRoomSize int
	minimumRoomSize int
	random          *rand.Rand
	canvas          *Map
	leafs           []*Leaf
}

func NewBSPTreeMapGenerator(width, height, maximumLeafSize, minimumLeafSize, maximumRoomSize, minimumRoomSize int, random *rand.Rand) *BSPTreeMapGenerator {
	return &BSPTreeMapGenerator{
		width:           width,
		height:          height,
		maximumLeafSize: maximumLeafSize,
		minimumLeafSize: minimumLeafSize,
		maximumRoomSize: maximumRoomSize,
		minimumRoomSize: minimumRoomSize,
		random:          random,
	}
}

func (generator *BSPTreeMapGenerator) CreateMap() *Map {
	generator.canvas = NewMap(generator.width, generator.height)
	generator.canvas.Clear(NewTile(TileBlock))
	root := NewLeaf(0, 0, generator.width, generator.height, generator.random)
	generator.leafs = append(generator.leafs, root)

	splitSuccessfully := true
	for {
		splitSuccessfully = false

		for _, leaf := range generator.leafs {
			if leaf.Left == nil && leaf.Right == nil {
				if leaf.Width > generator.maximumLeafSize || leaf.Height > generator.maximumLeafSize {
					if leaf.SplitLeaf(generator.minimumLeafSize) {
						generator.leafs = append(generator.leafs, leaf.Left, leaf.Right)
						splitSuccessfully = true
					}
				}
			}
		}

		if !splitSuccessfully {
			break
		}
	}

	generator.CreateRooms(root)
	return generator.canvas
}

func (generator *BSPTreeMapGenerator) CreateRoom(room image.Rectangle) {
	for x := room.Min.X + 1; x < room.Max.X; x++ {
		for y := room.Min.Y + 1; y < room.Max.Y; y++ {
			generator.canvas.SetTile(x, y, NewTile(TileEmpty))
		}
	}
}

func (generator *BSPTreeMapGenerator) CreateRooms(root *Leaf) {
	if root.Left != nil || root.Right != nil {
		if root.Left != nil {
			generator.CreateRooms(root.Left)
		}

		if root.Right != nil {
			generator.CreateRooms(root.Right)
		}

		if root.Left != nil && root.Right != nil {
			generator.CreateHall(root.Left.GetRoom(), root.Right.GetRoom())
		}
	} else {
		w := generator.random.Intn(int(math.Min(float64(generator.maximumRoomSize), float64((root.Width-1)-(generator.minimumRoomSize+1))) + float64(generator.minimumRoomSize)))
		h := generator.random.Intn(int(math.Min(float64(generator.minimumRoomSize), float64((root.Height-1)-(generator.minimumRoomSize+1))) + float64(generator.minimumRoomSize)))
		x := generator.random.Intn((root.X+(root.Width-1)-w)-root.X+1) + root.X
		y := generator.random.Intn((root.Y+(root.Height-1)-h)-root.Y+1) + root.Y

		root.Room = image.Rect(x, y, w, h)
		generator.CreateRoom(root.Room)
	}
}

func (generator *BSPTreeMapGenerator) CreateHall(room1, room2 image.Rectangle) {
	ax := int(math.Ceil(float64((room1.Min.X + room1.Max.X) / 2)))
	ay := int(math.Ceil(float64((room1.Min.Y + room1.Max.Y) / 2)))
	bx := int(math.Ceil(float64((room2.Min.X + room2.Max.X) / 2)))
	by := int(math.Ceil(float64((room2.Min.Y + room2.Max.Y) / 2)))

	if generator.random.Intn(1) > 0 {
		generator.MakeHorizontalTunnel(ax, bx, ay)
		generator.MakeVerticalTunnel(ay, by, by)
	} else {
		generator.MakeVerticalTunnel(ay, by, ay)
		generator.MakeHorizontalTunnel(ax, bx, by)
	}
}

func (generator *BSPTreeMapGenerator) MakeHorizontalTunnel(startX, endX, positionY int) {
	min := int(math.Min(float64(startX), float64(endX)))
	max := int(math.Max(float64(startX), float64(endX)))
	for x := min; x <= max; x++ {
		generator.canvas.SetTile(x, positionY, NewTile(TileEmpty))
	}
}

func (generator *BSPTreeMapGenerator) MakeVerticalTunnel(startY, endY, positionX int) {
	min := int(math.Min(float64(startY), float64(endY)))
	max := int(math.Max(float64(startY), float64(endY)))
	for y := min; y <= max; y++ {
		generator.canvas.SetTile(positionX, y, NewTile(TileEmpty))
	}
}
