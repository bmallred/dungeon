package dungeon

const (
	TileBlock = 0
	TileEmpty = 1
)

type Tile struct {
	Type int
}

func NewTile(tileType int) Tile {
	return Tile{
		Type: tileType,
	}
}
