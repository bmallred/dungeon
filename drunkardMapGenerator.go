package dungeon

import (
	"math"
	"math/rand"
)

type DrunkardsWalkMapGenerator struct {
	width                           int
	height                          int
	percentGoal                     float64
	walkIterations                  int
	weightedTowardCenter            float64
	weightedTowardPreviousDirection float64
	filledGoal                      float64
	drunkardX                       int
	drunkardY                       int
	previousDirection               int
	filled                          float64
	random                          *rand.Rand
	canvas                          *Map
}

func NewDrunkardsWalkMapGenerator(width, height, walkIterations int, percentGoal, weightedTowardCenter, weightedTowardPreviousDirection float64, random *rand.Rand) *DrunkardsWalkMapGenerator {
	return &DrunkardsWalkMapGenerator{
		width:                           width,
		height:                          height,
		percentGoal:                     percentGoal,
		walkIterations:                  int(math.Max(float64(walkIterations), float64(width*height*10))),
		weightedTowardCenter:            weightedTowardCenter,
		weightedTowardPreviousDirection: weightedTowardPreviousDirection,
		filledGoal:                      float64(width*height) * percentGoal,
		drunkardX:                       random.Intn((width-2)-(2+1)) + 2,
		drunkardY:                       random.Intn((height-2)-(2+1)) + 2,
		filled:                          0.0,
		random:                          random,
	}
}

func (generator *DrunkardsWalkMapGenerator) CreateMap() *Map {
	generator.canvas = NewMap(generator.width, generator.height)
	generator.canvas.Clear(NewTile(TileBlock))

	randomWalkIterations := generator.random.Intn(generator.walkIterations+1+1) + 1
	for iteration := 0; iteration < randomWalkIterations; iteration++ {
		generator.Walk(generator.width, generator.height)
		if generator.filled >= generator.filledGoal {
			break
		}
	}

	return generator.canvas
}

func (generator *DrunkardsWalkMapGenerator) Walk(width, height int) {
	// direction
	north := 1.0
	south := 1.0
	east := 1.0
	west := 1.0

	// weight the sandom walk against edges
	// drunkard is at far left side of map
	if float64(generator.drunkardX) < float64(generator.width)*0.25 {
		east += generator.weightedTowardCenter
	} else if float64(generator.drunkardX) > float64(generator.width)*0.75 {
		west += generator.weightedTowardCenter
	}

	// drunkard is at the top of the map
	if float64(generator.drunkardY) < float64(generator.height)*0.25 {
		south += generator.weightedTowardCenter
	} else if float64(generator.drunkardY) > float64(generator.height)*0.75 {
		north += generator.weightedTowardCenter
	}

	// weight the random walk in favor of the previous direction

	if generator.previousDirection == CardinalFourDirections[0] {
		north += generator.weightedTowardPreviousDirection
	}
	if generator.previousDirection == CardinalFourDirections[1] {
		east += generator.weightedTowardPreviousDirection
	}
	if generator.previousDirection == CardinalFourDirections[2] {
		west += generator.weightedTowardPreviousDirection
	}
	if generator.previousDirection == CardinalFourDirections[3] {
		south += generator.weightedTowardPreviousDirection
	}

	// normalize probabilities so they form a range from 0 to 1
	total := north + south + east + west
	north /= total
	south /= total
	east /= total
	west /= total

	// choose a direction
	direction := 0
	choice := generator.random.Float64()
	dx := 0
	dy := 0

	if 0 <= choice && choice < north {
		dx = 0
		dy = -1
		direction = 0
	} else if north <= choice && choice < (north+south) {
		dx = 0
		dy = 1
		direction = 3
	} else if (north+south) <= choice && choice < (north+south+east) {
		dx = 1
		dy = 0
		direction = 1
	} else {
		dx = -1
		dy = 0
		direction = 2
	}

	// walk
	if ((0 < generator.drunkardX) && (generator.drunkardX+dx < generator.width-1)) && ((0 < generator.drunkardY+dy) && (generator.drunkardY+dy < generator.height-1)) {
		generator.drunkardX += dx
		generator.drunkardY += dy

		if generator.canvas.GetTile(generator.drunkardX, generator.drunkardY).Type == TileBlock {
			generator.canvas.SetTile(generator.drunkardX, generator.drunkardY, NewTile(TileEmpty))
			generator.filled += 1
		}

		generator.previousDirection = direction
	}
}
