package dungeon

import (
	"image"
	"math/rand"

	"gitlab.com/bmallred/toolbox"
)

type HotelMapGenerator struct {
	width        int
	height       int
	maximumRooms int
	minimumRooms int
	random       *rand.Rand
	canvas       *Map
	Rooms        []image.Rectangle
}

func NewHotelMapGenerator(width, height, maximumRooms, minimumRooms int, random *rand.Rand) *HotelMapGenerator {
	return &HotelMapGenerator{
		width:        width,
		height:       height,
		maximumRooms: maximumRooms,
		minimumRooms: minimumRooms,
		random:       random,
	}
}

func (generator *HotelMapGenerator) padding() int {
	return toolbox.BetweenInt(generator.random, 5, 3)
}

func (generator *HotelMapGenerator) CreateMap() *Map {
	generator.canvas = NewMap(generator.width, generator.height)
	generator.canvas.Clear(NewTile(TileEmpty))

	root := image.Rect(0, 0, generator.width-1, generator.height-1)
	center := toolbox.RectangleCenter(root)
	elevator := image.Rect(center.X-generator.padding(), center.Y-generator.padding(), center.X+generator.padding(), center.Y+generator.padding())
	generator.Rooms = []image.Rectangle{root, elevator}

	// Carve out the elevator (always present)
	section1 := image.Rect(0, 0, center.X+generator.padding()-generator.padding(), center.Y-generator.padding()-generator.padding())
	section2 := image.Rect(center.X+generator.padding()+generator.padding(), 0, root.Max.X, center.Y+generator.padding()-generator.padding())
	section3 := image.Rect(center.X-generator.padding()+generator.padding(), center.Y+generator.padding()+generator.padding(), root.Max.X, root.Max.Y)
	section4 := image.Rect(0, center.Y-generator.padding()+generator.padding(), center.X-generator.padding()-generator.padding(), root.Max.Y)
	sections := []image.Rectangle{section1, section2, section3, section4}

	numberOfRooms := toolbox.BetweenInt(generator.random, generator.maximumRooms, generator.minimumRooms)

	iterations := 0
	maxIterations := generator.maximumRooms * generator.minimumRooms
	for iterations <= maxIterations {
		iterations += 1

		next := []image.Rectangle{}
		for _, section := range sections {
			next = append(next, generator.SplitRoom(section, generator.padding())...)
		}
		sections = next

		if numberOfRooms <= len(sections) {
			generator.Rooms = append(generator.Rooms, sections...)
			break
		}
	}

	// If a maximum was reached then try again
	if iterations > maxIterations {
		return generator.CreateMap()
	}

	// Draw the rooms
	for _, room := range generator.Rooms {
		generator.FrameRoom(room)
	}

	for _, room := range generator.Rooms {
		generator.BuildDoor(room)
	}

	return generator.canvas
}

func (generator *HotelMapGenerator) SplitRoom(room image.Rectangle, padding int) []image.Rectangle {
	rooms := []image.Rectangle{}
	horizontal := toolbox.BetweenInt(generator.random, 1, 0) == 1
	halfpad := padding / 2
	split := toolbox.BetweenInt(generator.random, 6, 3)

	if horizontal {
		position := toolbox.BetweenInt(generator.random, room.Max.Y, room.Max.Y/split)
		if max(1, position-halfpad)-room.Min.Y < 4 || room.Max.Y-min(generator.height-1, position+halfpad) < 4 {
			return []image.Rectangle{room}
		}

		a := image.Rect(room.Min.X, room.Min.Y, room.Max.X, max(1, position-halfpad))
		b := image.Rect(room.Min.X, min(generator.height-1, position+halfpad), room.Max.X, room.Max.Y)
		rooms = append(rooms, a, b)
	} else {
		position := toolbox.BetweenInt(generator.random, room.Max.X, room.Max.X/split)
		if max(1, position-halfpad)-room.Min.X < 4 || room.Max.X-min(generator.width-1, position+halfpad) < 4 {
			return []image.Rectangle{room}
		}

		a := image.Rect(room.Min.X, room.Min.Y, max(1, position-halfpad), room.Max.Y)
		b := image.Rect(min(generator.width-1, position+halfpad), room.Min.Y, room.Max.X, room.Max.Y)
		rooms = append(rooms, a, b)
	}

	return rooms
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func max(a, b int) int {
	if a < b {
		return b
	}
	return a
}

func (generator *HotelMapGenerator) BuildDoor(room image.Rectangle) {
	// Maximum of 5 attempts with each attempt increasing the chance a door is added to a wall.
	for i := 0; i < 5; i++ {
		door := false

		// North
		chance := toolbox.BetweenInt(generator.random, 1, 0) == 0
		if chance && room.Min.Y != 0 {
			x := toolbox.BetweenInt(generator.random, room.Max.X-1, room.Min.X+1)
			y := room.Min.Y
			generator.canvas.SetTile(x, y, NewTile(TileEmpty))

			door = true
			if i > 0 {
				return
			}
		}

		// East
		chance = toolbox.BetweenInt(generator.random, 1, 0) == 0
		if chance && room.Max.X != generator.width-1 {
			x := room.Max.X
			y := toolbox.BetweenInt(generator.random, room.Max.Y-1, room.Min.Y+1)
			generator.canvas.SetTile(x, y, NewTile(TileEmpty))

			door = true
			if i > 0 {
				return
			}
		}

		// South
		chance = toolbox.BetweenInt(generator.random, 1, 0) == 0
		if chance && room.Max.Y != generator.height-1 {
			x := toolbox.BetweenInt(generator.random, room.Max.X-1, room.Min.X+1)
			y := room.Max.Y
			generator.canvas.SetTile(x, y, NewTile(TileEmpty))

			door = true
			if i > 0 {
				return
			}
		}

		// West
		chance = toolbox.BetweenInt(generator.random, 1, 0) == 0
		if chance && room.Min.X != 0 {
			x := room.Min.X
			y := toolbox.BetweenInt(generator.random, room.Max.Y-1, room.Min.Y+1)
			generator.canvas.SetTile(x, y, NewTile(TileEmpty))

			door = true
			if i > 0 {
				return
			}
		}

		if door {
			return
		}
	}
}

func (generator *HotelMapGenerator) FrameRoom(room image.Rectangle) {
	for x := room.Min.X; x < room.Max.X; x++ {
		for y := room.Min.Y; y < room.Max.Y; y++ {
			generator.canvas.SetTile(x, y, NewTile(TileEmpty))
		}
	}
	for x := room.Min.X; x <= room.Max.X; x++ {
		generator.canvas.SetTile(x, room.Min.Y, NewTile(TileBlock))
		generator.canvas.SetTile(x, room.Max.Y, NewTile(TileBlock))
	}
	for y := room.Min.Y; y <= room.Max.Y; y++ {
		generator.canvas.SetTile(room.Min.X, y, NewTile(TileBlock))
		generator.canvas.SetTile(room.Max.X, y, NewTile(TileBlock))
	}
}
