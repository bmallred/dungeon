package main

import (
	"bytes"
	"errors"
	"image"
	"image/color"
	_ "image/png"
	"log"
	"math/rand"
	"os"
	"time"

	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/inpututil"
	"gitlab.com/bmallred/dungeon"
	"gitlab.com/bmallred/dungeon/assets"
	"gitlab.com/bmallred/ecs"
)

const (
	width  = 50
	height = 50
	scale  = 8
)

var (
	r        *rand.Rand
	entities []*ecs.Entity
	textures map[string]*ebiten.Image
	systems  []ecs.System
	canvas   *dungeon.Map
	last     time.Time
	window   *ebiten.Image
	stop     bool = false
	current  int
)

var generators = map[int]func() *dungeon.Map{
	0: func() *dungeon.Map {
		log.Println("Generating border-only-map")
		return dungeon.NewBorderOnlyMapGenerator(width, height).CreateMap()
	},
	1: func() *dungeon.Map {
		log.Println("Generating bsp-tree-map")
		return dungeon.NewBSPTreeMapGenerator(width, height, 24, 10, 15, 6, r).CreateMap()
	},
	2: func() *dungeon.Map {
		log.Println("Generating new-city-map")
		return dungeon.NewCityMapGenerator(width, height, 30, 8, 16, 8, r).CreateMap()
	},
	3: func() *dungeon.Map {
		log.Println("Generating dfs-maze-map")
		return dungeon.NewDFSMazeMapGenerator(width, height, r).CreateMap()
	},
	4: func() *dungeon.Map {
		log.Println("Generating drunkards-walk-map")
		return dungeon.NewDrunkardsWalkMapGenerator(width, height, 50000, 0.3, 0.15, 0.7, r).CreateMap()
	},
	5: func() *dungeon.Map {
		log.Println("Generating tunneling-maze-map")
		return dungeon.NewTunnelingMazeMapGenerator(width, height, 666, r).CreateMap()
	},
	6: func() *dungeon.Map {
		log.Println("Generating tunneling-with-rooms-map")
		return dungeon.NewTunnelingWithRoomsMapGenerator(width, height, 30, 15, 6, r).CreateMap()
	},
	7: func() *dungeon.Map {
		log.Println("Generating hotel-map")
		m := dungeon.NewHotelMapGenerator(width, height, 30, 15, r).CreateMap()
		return m
	},
}

func main() {
	setup()
	run()
	unload()

	os.Exit(0)
}

func createCanvas() {
	canvas = generators[current]()
}

func setup() {
	seed := time.Now().UnixNano()
	log.Printf("seed: %d", seed)
	r = rand.New(rand.NewSource(seed))
	// current = r.Intn(len(generators))
	current = 7
	createCanvas()

	// Declare systems
	systems = []ecs.System{
		// &ReloadSystem{game: game, updatedAt: time.Now()},
		&ControlSystem{},
		&RenderSystem{},
	}
}

func preload() {
	textures = map[string]*ebiten.Image{}

	imagefiles := []string{"assets/empty.png", "assets/wall.png"}
	for _, name := range imagefiles {
		// Pull assets from memory
		png, err := assets.Read(name, true)
		if err != nil {
			log.Println("failed to read image bytes", err)
			continue
		}

		// Decode the bytes
		img, _, err := image.Decode(bytes.NewReader(png))
		if err != nil {
			log.Println("failed to load image", err)
			continue
		}

		texture, err := ebiten.NewImageFromImage(img, ebiten.FilterDefault)
		if err != nil {
			log.Println("failed to load texture", err)
			continue
		}

		textures[name] = texture
	}
}

func run() {
	// This is here if you want to unpack the assets
	// assets.Unpack()

	// Preload assets once game context has been established.
	preload()

	ebiten.SetRunnableInBackground(true)
	if err := ebiten.Run(update, width, height, scale, "Dungeon"); err != nil {
		log.Println(err)
	}
}

func update(screen *ebiten.Image) error {
	window = screen

	// Update the timer to calculate the next delta.
	last = time.Now()

	// Loop through all systems
	for _, system := range systems {
		system.Update(time.Since(last), entities)
	}

	if stop {
		return errors.New("Game over")
	}

	// ebitenutil.DebugPrint(screen, fmt.Sprintf("TPS: %0.2f", ebiten.CurrentTPS()))

	return nil
}

func unload() {
	for _, texture := range textures {
		texture.Dispose()
	}
}

func repeatingKeyPressed(key ebiten.Key) bool {
	const (
		delay    = 30
		interval = 3
	)

	d := inpututil.KeyPressDuration(key)
	if d == 1 {
		return true
	}

	if d >= delay && (d-delay)%interval == 0 {
		return true
	}

	return false
}

// ControlSystem provides handling of player inputs.
type ControlSystem struct{}

// Update entities based on player inputs.
func (system *ControlSystem) Update(delta time.Duration, entities []*ecs.Entity) {
	if repeatingKeyPressed(ebiten.KeySpace) {
		createCanvas()
	}

	if repeatingKeyPressed(ebiten.KeyUp) {
		total := len(generators)
		current += 1
		if current >= total {
			current = 0
		}
		createCanvas()
	}

	if repeatingKeyPressed(ebiten.KeyDown) {
		current -= 1
		if current < 0 {
			current = len(generators) - 1
		}
		createCanvas()
	}

	if ebiten.IsKeyPressed(ebiten.KeyEscape) || ebiten.IsKeyPressed(ebiten.KeyQ) {
		stop = true
	}
}

type RenderSystem struct{}

func (system *RenderSystem) Update(delta time.Duration, entities []*ecs.Entity) {
	if ebiten.IsDrawingSkipped() {
		return
	}

	empty := textures["assets/empty.png"]
	wall := textures["assets/wall.png"]

	window.Fill(color.RGBA{0xf0, 0xf0, 0xf0, 0xf0})
	for _, tileData := range canvas.GetAllTiles() {
		var texture *ebiten.Image
		if tileData.Tile.Type == dungeon.TileEmpty {
			texture = empty
		} else {
			texture = wall
		}

		options := &ebiten.DrawImageOptions{}
		options.GeoM.Translate(float64(tileData.X), float64(tileData.Y))

		window.DrawImage(texture, options)
	}
}
