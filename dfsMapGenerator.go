package dungeon

import "math/rand"

type DFSMazeMapGenerator struct {
	width  int
	height int
	random *rand.Rand
	canvas *Map
}

func NewDFSMazeMapGenerator(width, height int, random *rand.Rand) *DFSMazeMapGenerator {
	return &DFSMazeMapGenerator{
		width:  width,
		height: height,
		random: random,
	}
}

func (generator *DFSMazeMapGenerator) CreateMap() *Map {
	generator.canvas = NewMap(generator.width, generator.height)
	generator.canvas.Clear(NewTile(TileBlock))

	generator.CreateMaze(1, 1)

	return generator.canvas
}

func (generator *DFSMazeMapGenerator) CreateMaze(i, j int) {
	visitOrder := []int{0, 1, 2, 3}

	// out of boundary
	if i < 1 || j < 1 || i >= generator.width-1 || j >= generator.height-1 {
		return
	}

	// visited, go back the coming direction, return
	if generator.canvas.GetTile(i, j).Type != TileBlock {
		return
	}

	// some neighbors are visited in addition to the coming direction, return
	// this is to avoid circles in the maze
	if generator.CountVisitedNeighbor(i, j) > 1 {
		return
	}

	generator.canvas.SetTile(i, j, NewTile(TileEmpty))

	// shuffle the visit order
	visitOrder = generator.Shuffle(visitOrder)
	for k := 0; k < 4; k++ {
		ni := i + FourDirections[visitOrder[k]][0]
		nj := j + FourDirections[visitOrder[k]][1]
		generator.CreateMaze(ni, nj)
	}
}

func (generator *DFSMazeMapGenerator) CountVisitedNeighbor(i, j int) int {
	count := 0

	for _, direction := range FourDirections {
		ni := i + direction[0]
		nj := j + direction[1]
		if ni < 0 || nj < 0 || ni >= generator.width || nj >= generator.height {
			continue
		}

		// visited
		if generator.canvas.GetTile(ni, nj).Type != TileBlock {
			count += 1
		}
	}

	return count
}

func (generator *DFSMazeMapGenerator) Shuffle(list []int) []int {
	n := len(list)

	for n > 1 {
		n -= 1
		k := generator.random.Intn(n + 1)
		value := list[k]
		list[k] = list[n]
		list[n] = value
	}

	return list
}
