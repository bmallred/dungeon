package dungeon

import (
	"image"
	"math"
	"math/rand"
)

type CityMapGenerator struct {
	width           int
	height          int
	maximumLeafSize int
	minimumLeafSize int
	maximumRoomSize int
	minimumRoomSize int
	random          *rand.Rand
	canvas          *Map
	leafs           []*Leaf
	rooms           []image.Rectangle
}

func NewCityMapGenerator(width, height, maximumLeafSize, minimumLeafSize, maximumRoomSize, minimumRoomSize int, random *rand.Rand) *CityMapGenerator {
	return &CityMapGenerator{
		width:           width,
		height:          height,
		maximumLeafSize: maximumLeafSize,
		minimumLeafSize: minimumLeafSize,
		maximumRoomSize: maximumRoomSize,
		minimumRoomSize: minimumRoomSize,
		random:          random,
	}
}

func (generator *CityMapGenerator) CreateMap() *Map {
	generator.canvas = NewMap(generator.width, generator.height)
	generator.canvas.Clear(NewTile(TileEmpty))

	root := NewLeaf(1, 1, generator.width-1, generator.height-1, generator.random)
	generator.leafs = append(generator.leafs, root)

	splitSuccessfully := true
	for {
		splitSuccessfully = false

		for _, leaf := range generator.leafs {
			if leaf.Left == nil && leaf.Right == nil {
				if leaf.Width > generator.maximumLeafSize || leaf.Height > generator.maximumLeafSize {
					if leaf.SplitLeaf(generator.minimumLeafSize) {
						generator.leafs = append(generator.leafs, leaf.Left, leaf.Right)
						splitSuccessfully = true
					}
				}
			}
		}

		if !splitSuccessfully {
			break
		}
	}

	generator.CreateRooms(root)
	generator.CreateDoors()
	return generator.canvas
}

func (generator *CityMapGenerator) CreateRooms(root *Leaf) {
	if root.Left != nil || root.Right != nil {
		if root.Left != nil {
			generator.CreateRooms(root.Left)
		}

		if root.Right != nil {
			generator.CreateRooms(root.Right)
		}

		if root.Left != nil && root.Right != nil {
			generator.CreateHall(root.Left.GetRoom(), root.Right.GetRoom())
		}
	} else {
		w := generator.random.Intn(int(math.Min(float64(generator.maximumRoomSize), float64((root.Width-1)-(generator.minimumRoomSize+1))) + float64(generator.minimumRoomSize)))
		h := generator.random.Intn(int(math.Min(float64(generator.minimumRoomSize), float64((root.Height-1)-(generator.minimumRoomSize+1))) + float64(generator.minimumRoomSize)))
		x := generator.random.Intn((root.X+(root.Width-1)-w)-root.X+1) + root.X
		y := generator.random.Intn((root.Y+(root.Height-1)-h)-root.Y+1) + root.Y

		root.Room = image.Rect(x, y, w, h)
		generator.CreateRoom(root.Room)
	}
}

func (generator *CityMapGenerator) CreateRoom(room image.Rectangle) {
	generator.rooms = append(generator.rooms, room)

	// Build walls
	for x := room.Min.X; x < room.Max.X; x++ {
		for y := room.Min.Y; y < room.Max.Y; y++ {
			generator.canvas.SetTile(x, y, NewTile(TileBlock))
		}
	}

	for x := room.Min.X + 1; x < room.Max.X; x++ {
		for y := room.Min.Y + 1; y < room.Max.Y; y++ {
			generator.canvas.SetTile(x, y, NewTile(TileEmpty))
		}
	}
}

func (generator *CityMapGenerator) CreateHall(room1, room2 image.Rectangle) {
	found1 := false
	found2 := false
	for _, room := range generator.rooms {
		if room.Eq(room1) {
			found1 = true
		}

		if room.Eq(room2) {
			found2 = true
		}
	}

	if !found1 {
		generator.rooms = append(generator.rooms, room1)
	}

	if !found2 {
		generator.rooms = append(generator.rooms, room2)
	}
}

func (generator *CityMapGenerator) CreateDoors() {
	for _, room := range generator.rooms {
		cx := int(math.Ceil(float64((room.Min.X + room.Max.X) / 2)))
		cy := int(math.Ceil(float64((room.Min.Y + room.Max.Y) / 2)))

		// TODO Cardinal directions
		door := []int{0, 0}
		randomDirection := generator.random.Intn(3)
		switch randomDirection {
		case 0:
			// North
			door = []int{cx, room.Max.Y}
			break
		case 1:
			// East
			door = []int{room.Min.X, cy}
			break
		case 2:
			// West
			door = []int{room.Max.X, cy}
			break
		case 3:
			// South
			door = []int{cx, room.Min.Y}
			break
		}

		generator.canvas.SetTile(door[0], door[1], NewTile(TileEmpty))
	}
}
