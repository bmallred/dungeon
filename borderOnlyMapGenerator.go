package dungeon

type BorderOnlyMapGenerator struct {
	width  int
	height int
}

func NewBorderOnlyMapGenerator(width, height int) *BorderOnlyMapGenerator {
	return &BorderOnlyMapGenerator{
		width:  width,
		height: height,
	}
}

func (generator *BorderOnlyMapGenerator) CreateMap() *Map {
	canvas := NewMap(generator.width, generator.height)
	canvas.Clear(NewTile(TileEmpty))

	for _, tileData := range canvas.GetTilesInRows(0, generator.height-1) {
		canvas.SetTile(tileData.X, tileData.Y, NewTile(TileBlock))
	}

	for _, tileData := range canvas.GetTilesInColumns(0, generator.width-1) {
		canvas.SetTile(tileData.X, tileData.Y, NewTile(TileBlock))
	}

	return canvas
}
