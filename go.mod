module gitlab.com/bmallred/dungeon

go 1.13

require (
	github.com/hajimehoshi/ebiten v1.10.3
	gitlab.com/bmallred/ecs v0.1.0
	gitlab.com/bmallred/toolbox v0.1.4
)
