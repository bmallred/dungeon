package dungeon

type MapGenerator interface {
	CreateMap() *Map
}
