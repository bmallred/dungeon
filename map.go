package dungeon

import (
	"math"
)

var CardinalFourDirections []int = []int{
	0, // North
	1, // East
	2, // West
	3, // South
}

var FourDirections [][]int = [][]int{
	[]int{0, 1},  // North
	[]int{0, -1}, // South
	[]int{1, 0},  // East
	[]int{-1, 0}, // West
}

var NineDirections [][]int = [][]int{
	[]int{0, -1},  // North
	[]int{0, 1},   // South
	[]int{1, 0},   // East
	[]int{-1, 0},  // West
	[]int{1, -1},  // Northeast
	[]int{-1, -1}, // Northwest
	[]int{-1, 1},  // Southwest
	[]int{1, 1},   // Southeast
	[]int{0, 0},   // Center
}

type TileData struct {
	X    int
	Y    int
	Tile *Tile
}

type Map struct {
	Width   int
	Height  int
	Terrain [][]Tile
}

func NewMap(width, height int) *Map {
	terrain := [][]Tile{}
	for x := 0; x < width; x++ {
		arr := []Tile{}
		for y := 0; y < height; y++ {
			arr = append(arr, NewTile(TileEmpty))
		}
		terrain = append(terrain, arr)
	}

	return &Map{
		Width:   width,
		Height:  height,
		Terrain: terrain,
	}
}

func (m *Map) InBounds(x, y int) bool {
	if x < 0 || y < 0 || x >= m.Width || y >= m.Height {
		return false
	}
	return true
}

func (m *Map) IsOpaque(x, y int) bool {
	tile := m.GetTile(x, y)
	if tile.Type == TileBlock {
		return true
	}
	return false
}

func (m *Map) GetTile(x, y int) *Tile {
	if x < 0 || y < 0 || x >= m.Width || y >= m.Height {
		return nil
	}
	return &m.Terrain[x][y]
}

func (m *Map) SetTile(x, y int, tile Tile) {
	m.Terrain[x][y] = tile
}

func (m *Map) IsBorderTile(x, y int) bool {
	return x == 0 || x == m.Width-1 || y == 0 || y == m.Height-1
}

func (m *Map) ClampX(x int) int {
	if x < 0 {
		return 0
	}

	if x > m.Width-1 {
		return m.Width - 1
	}

	return x
}

func (m *Map) ClampY(y int) int {
	if y < 0 {
		return 0
	}

	if y > m.Height-1 {
		return m.Height - 1
	}

	return y
}

func (m *Map) Clear(tile Tile) {
	for y := 0; y < m.Height; y++ {
		for x := 0; x < m.Width; x++ {
			m.SetTile(x, y, tile)
		}
	}
}

func (m *Map) Clone() *Map {
	clone := NewMap(m.Width, m.Height)
	for _, data := range m.GetAllTiles() {
		clone.SetTile(data.X, data.Y, *data.Tile)
	}
	return clone
}

func (m *Map) GetAllTiles() []TileData {
	arr := []TileData{}
	for y := 0; y < m.Height; y++ {
		for x := 0; x < m.Width; x++ {
			arr = append(arr, TileData{
				X:    x,
				Y:    y,
				Tile: m.GetTile(x, y),
			})
		}
	}
	return arr
}

func (m *Map) GetTilesInRows(rows ...int) []TileData {
	arr := []TileData{}
	for _, y := range rows {
		for x := 0; x < m.Width; x++ {
			arr = append(arr, TileData{
				X:    x,
				Y:    y,
				Tile: m.GetTile(x, y),
			})
		}
	}
	return arr
}

func (m *Map) GetTilesInColumns(columns ...int) []TileData {
	arr := []TileData{}
	for _, x := range columns {
		for y := 0; y < m.Height; y++ {
			arr = append(arr, TileData{
				X:    x,
				Y:    y,
				Tile: m.GetTile(x, y),
			})
		}
	}
	return arr
}

func (m *Map) GetTilesInSquare(centerX, centerY, distance int) []TileData {
	minX := int(math.Max(0, float64(centerX-distance)))
	maxX := int(math.Min(float64(m.Width-1), float64(centerX+distance)))
	minY := int(math.Max(0, float64(centerY-distance)))
	maxY := int(math.Min(float64(m.Height-1), float64(centerY+distance)))

	arr := []TileData{}
	for y := minY; y <= maxY; y++ {
		for x := minX; x <= maxX; x++ {
			arr = append(arr, TileData{
				X:    x,
				Y:    y,
				Tile: m.GetTile(x, y),
			})
		}
	}
	return arr
}

func (m *Map) GetCellsAlongLine(originX, originY, destinationX, destinationY int) []TileData {
	originX = m.ClampX(originX)
	originY = m.ClampY(originY)
	destinationX = m.ClampX(destinationX)
	destinationY = m.ClampY(destinationY)

	dx := int(math.Abs(float64(destinationX - originX)))
	dy := int(math.Abs(float64(destinationY - originY)))
	err := dx - dy

	sx := -1
	if originX < destinationX {
		sx = 1
	}

	sy := -1
	if originY < destinationY {
		sy = 1
	}

	arr := []TileData{}
	for {
		arr = append(arr, TileData{
			X:    originX,
			Y:    originY,
			Tile: m.GetTile(originX, originY),
		})

		if originX == destinationX && originY == destinationY {
			break
		}

		e2 := 2 * err
		if e2 > -dy {
			err = err - dy
			originX = originX + sx
		}

		if e2 < dx {
			err = err + dx
			originY = originY + sy
		}
	}
	return arr
}
