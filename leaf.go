package dungeon

import (
	"image"
	"math/rand"
)

type Leaf struct {
	Width  int
	Height int
	Left   *Leaf
	Right  *Leaf
	random *rand.Rand
	X      int
	Y      int
	Room   image.Rectangle
	room1  image.Rectangle
	room2  image.Rectangle
}

func NewLeaf(x, y, width, height int, random *rand.Rand) *Leaf {
	return &Leaf{
		Width:  width,
		Height: height,
		X:      x,
		Y:      y,
		random: random,
	}
}

func (l *Leaf) SplitLeaf(minimumSize int) bool {
	if l.Left != nil || l.Right != nil {
		return false
	}

	// Determine the direction of the split
	// If the width of the leaf is 25% larger than the height, split the leaf vertically
	// If the height of the leaf is 25% larger than the width, split the leaf horizontally
	// Otherwise, choose the direction at random
	splitHorizontally := false
	if l.random.Intn(1) > 0 {
		splitHorizontally = true
	}

	horizontalFactor := float64(l.Width) / float64(l.Height)
	verticalFactor := float64(l.Height) / float64(l.Width)
	if horizontalFactor >= 1.25 {
		splitHorizontally = false
	} else if verticalFactor >= 1.25 {
		splitHorizontally = true
	}

	max := 0
	if splitHorizontally {
		max = l.Height - minimumSize
	} else {
		max = l.Width - minimumSize
	}

	if max <= minimumSize {
		return false
	}

	split := l.random.Intn(max-minimumSize+1) + minimumSize
	if splitHorizontally {
		l.Left = NewLeaf(l.X, l.Y, l.Width, split, l.random)
		l.Right = NewLeaf(l.X, l.Y+split, l.Width, l.Height-split, l.random)
	} else {
		l.Left = NewLeaf(l.X, l.Y, split, l.Height, l.random)
		l.Right = NewLeaf(l.X+split, l.Y, l.Width-split, l.Height, l.random)
	}

	return true
}

func (l *Leaf) GetRoom() image.Rectangle {
	zero := image.Rectangle{}
	if !l.Room.Eq(zero) {
		return l.Room
	} else {
		if l.Left != nil {
			l.room1 = l.Left.GetRoom()
		}

		if l.Right != nil {
			l.room2 = l.Right.GetRoom()
		}
	}

	if l.Left == nil && l.Right == nil {
		return zero
	}

	if l.room2.Eq(zero) {
		return l.room1
	}

	if l.room1.Eq(zero) {
		return l.room2
	}

	if l.random.Intn(1) > 0 {
		return l.room1
	}
	return l.room2
}
