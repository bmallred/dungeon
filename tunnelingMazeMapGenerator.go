package dungeon

import (
	"image"
	"math"
	"math/rand"
)

const (
	jumpTile = 2
)

type TunnelingMazeMapGenerator struct {
	width             int
	height            int
	maximumTileWidth  int
	maximumTileHeight int
	magicNumber       int
	random            *rand.Rand
	tiles             [][]int
	canvas            *Map
}

func NewTunnelingMazeMapGenerator(width, height, magicNumber int, random *rand.Rand) *TunnelingMazeMapGenerator {
	// calculate the maximum width and height
	maximumTileWidth := width / jumpTile
	maximumTileHeight := height / jumpTile

	// init the tiles
	tiles := [][]int{}
	for x := 0; x < maximumTileWidth; x++ {
		arr := []int{}
		for y := 0; y < maximumTileHeight; y++ {
			arr = append(arr, 0)
		}
		tiles = append(tiles, arr)
	}

	return &TunnelingMazeMapGenerator{
		width:             width,
		height:            height,
		maximumTileWidth:  maximumTileWidth,
		maximumTileHeight: maximumTileHeight,
		random:            random,
		tiles:             tiles,
	}
}

func (generator *TunnelingMazeMapGenerator) CreateMap() *Map {
	generator.canvas = NewMap(generator.width, generator.height)
	generator.canvas.Clear(NewTile(TileBlock))

	totalCells := generator.maximumTileWidth * generator.maximumTileHeight
	rx := generator.maximumTileWidth / 2
	ry := generator.maximumTileHeight / 2
	count := 0

	generator.tiles[rx][ry] = 1
	visitedCells := 1

	for visitedCells < totalCells {
		count += 1
		if count < generator.magicNumber {
			generator.FillCells()
		}

		// use direction lookup table for more generic dig function
		direction := FourDirections[generator.random.Intn(len(FourDirections))]
		isInRange := generator.IsInRange(rx*jumpTile+direction[0], ry*jumpTile+direction[1])

		x := rx + direction[0]
		y := ry + direction[1]
		if isInRange && generator.tiles[x][y] == 0 {
			generator.LinkCells(rx*jumpTile, ry*jumpTile, x*jumpTile, y*jumpTile)
			rx = x
			ry = y
		} else {
			for generator.tiles[rx][ry] != 1 {
				rx = generator.random.Intn(generator.maximumTileWidth)
				ry = generator.random.Intn(generator.maximumTileHeight)
			}
		}

		generator.tiles[rx][ry] = 1
		generator.canvas.SetTile(rx*jumpTile, ry*jumpTile, NewTile(TileEmpty))
		visitedCells += 1
	}

	return generator.canvas
}

func (generator *TunnelingMazeMapGenerator) FillCells() {
	for x := 0; x < generator.maximumTileWidth; x++ {
		for y := 0; y < generator.maximumTileHeight; y++ {
			if generator.tiles[x][y] == 1 {
				generator.canvas.SetTile(x*jumpTile, y*jumpTile, NewTile(TileEmpty))
			}
		}
	}
}

func (generator *TunnelingMazeMapGenerator) LinkCells(x0, y0, x1, y1 int) {
	tileX := x0
	tileY := y0

	for tileX != x1 {
		if x0 > x1 {
			tileX -= 1
		} else {
			tileX += 1
		}

		if generator.IsInRange(tileX, tileY) {
			generator.canvas.SetTile(tileX, tileY, NewTile(TileEmpty))
		}
	}

	for tileY != y1 {
		if y0 > y1 {
			tileY -= 1
		} else {
			tileY += 1
		}

		if generator.IsInRange(tileX, tileY) {
			generator.canvas.SetTile(tileX, tileY, NewTile(TileEmpty))
		}
	}
}

func (generator *TunnelingMazeMapGenerator) IsInRange(x, y int) bool {
	if x > 2 && y > 2 && x < generator.width-2 && y < generator.height-2 {
		return true
	}
	return false
}

type TunnelingWithRoomsMapGenerator struct {
	width           int
	height          int
	maximumRooms    int
	maximumRoomSize int
	minimumRoomSize int
	random          *rand.Rand
	canvas          *Map
}

func NewTunnelingWithRoomsMapGenerator(width, height, maximumRooms, maximumRoomSize, minimumRoomSize int, random *rand.Rand) *TunnelingWithRoomsMapGenerator {
	return &TunnelingWithRoomsMapGenerator{
		width:           width,
		height:          height,
		maximumRooms:    maximumRooms,
		maximumRoomSize: maximumRoomSize,
		minimumRoomSize: minimumRoomSize,
		random:          random,
	}
}

func (generator *TunnelingWithRoomsMapGenerator) CreateMap() *Map {
	generator.canvas = NewMap(generator.width, generator.height)
	generator.canvas.Clear(NewTile(TileBlock))

	rooms := []image.Rectangle{}

	for r := 0; r < generator.maximumRooms; r++ {
		rw := generator.random.Intn(generator.maximumRoomSize-generator.minimumRoomSize+1) + generator.minimumRoomSize
		rh := generator.random.Intn(generator.maximumRoomSize-generator.minimumRoomSize+1) + generator.minimumRoomSize
		rx := generator.random.Intn(generator.width - rw)
		ry := generator.random.Intn(generator.height - rh)

		newRoom := image.Rect(rx, ry, rw, rh)
		intersects := false
		for _, room := range rooms {
			if newRoom.Overlaps(room) {
				intersects = true
				break
			}
		}

		if !intersects {
			rooms = append(rooms, newRoom)
		}
	}

	for _, room := range rooms {
		generator.MakeRoom(room)
	}

	for r := 0; r < len(rooms); r++ {
		if r == 0 {
			continue
		}

		// previous room center
		px := int(math.Ceil(float64((rooms[r-1].Min.X + rooms[r-1].Max.X) / 2)))
		py := int(math.Ceil(float64((rooms[r-1].Min.Y + rooms[r-1].Max.Y) / 2)))

		// current room center
		cx := int(math.Ceil(float64((rooms[r].Min.X + rooms[r].Max.X) / 2)))
		cy := int(math.Ceil(float64((rooms[r].Min.Y + rooms[r].Max.Y) / 2)))

		if generator.random.Intn(1) == 0 {
			generator.MakeHorizontalTunnel(px, cx, py)
			generator.MakeVerticalTunnel(py, cy, cx)
		} else {
			generator.MakeVerticalTunnel(py, cy, px)
			generator.MakeHorizontalTunnel(px, cx, cy)
		}
	}

	return generator.canvas
}

func (generator *TunnelingWithRoomsMapGenerator) MakeRoom(room image.Rectangle) {
	for x := room.Min.X + 1; x < room.Max.X; x++ {
		for y := room.Min.Y + 1; y < room.Max.Y; y++ {
			generator.canvas.SetTile(x, y, NewTile(TileEmpty))
		}
	}
}

func (generator *TunnelingWithRoomsMapGenerator) MakeHorizontalTunnel(startX, endX, positionY int) {
	min := int(math.Min(float64(startX), float64(endX)))
	max := int(math.Max(float64(startX), float64(endX)))
	for x := min; x <= max; x++ {
		generator.canvas.SetTile(x, positionY, NewTile(TileEmpty))
	}
}

func (generator *TunnelingWithRoomsMapGenerator) MakeVerticalTunnel(startY, endY, positionX int) {
	min := int(math.Min(float64(startY), float64(endY)))
	max := int(math.Max(float64(startY), float64(endY)))
	for y := min; y <= max; y++ {
		generator.canvas.SetTile(positionX, y, NewTile(TileEmpty))
	}
}
